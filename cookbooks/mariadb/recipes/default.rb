#
# Cookbook:: mariadb
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

package 'mariadb-server' do
  action :install
end

service 'mariadb' do
  action [:start, :enable]
end

bash 'create_user' do
  code <<-EOH
    mysql -e "CREATE DATABASE wordpress;"
    mysql -e "CREATE USER wordpress@'%' IDENTIFIED BY 'wordpress';"
    mysql -e "GRANT ALL PRIVILEGES ON wordpress.* TO wordpress@'%';"
    mysql -e "FLUSH PRIVILEGES;"
    EOH
end
