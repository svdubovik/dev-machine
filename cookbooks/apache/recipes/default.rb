#
# Cookbook:: apache
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.

include_recipe 'selinux::permissive'

node.default['wp']['version'] = '4.9.5'
node.default['wp']['root_dir'] = '/var/www/html'
package_url = "https://wordpress.org/wordpress-#{node['wp']['version']}.tar.gz"
package_file = ::File.join(Chef::Config[:file_cache_path], ::File.basename(package_url))

package 'git' do
  action :install
end

package 'httpd' do
  action :install
end

service 'httpd' do
  action [:enable, :start]
end

package 'php' do
  action :install
  notifies :reload, 'service[httpd]', :immediately
end

package 'php-mysql' do
  action :install
  notifies :reload, 'service[httpd]', :immediately
end

remote_file 'download_package' do
  path package_file
  source package_url
  not_if { ::File.exist?(package_file) }
  notifies :run, 'bash[extract_wp_core]', :immediately
end

bash 'extract_wp_core' do
  code <<-EOH
    rm -rf /tmp/wordpress
    tar -xzf #{package_file} -C /tmp
    find /tmp/wordpress -mindepth 1 -maxdepth 1 -name 'wp-content' -or -exec cp -a {} #{node['wp']['root_dir']} \\;
    EOH
  action :nothing
  notifies :reload, 'service[httpd]', :immediately
end

db = search(:node, "name:#{node.chef_environment}-db").first

template ::File.join(node['wp']['root_dir'], 'wp-config.php') do
  source 'wp-config.php.erb'
  variables(ip: db['ipaddress'])
  action :create
  notifies :reload, 'service[httpd]', :immediately
end
